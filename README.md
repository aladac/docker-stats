# docker-stats

* [Homepage](https://rubygems.org/gems/docker-stats)
* [Documentation](http://rubydoc.info/gems/docker-stats/frames)
* [Email](mailto:adam at saiden.pl)

[![Code Climate GPA](https://codeclimate.com/github//docker-stats/badges/gpa.svg)](https://codeclimate.com/github//docker-stats)

## Description

Docker Stats access for Ruby

## Features

## Examples

    require 'docker/stats'

## Requirements

## Install

    $ gem install docker-stats

## Copyright

Copyright (c) 2016 Adam Ladachowski

See {file:LICENSE.txt} for details.
