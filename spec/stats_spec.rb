require 'spec_helper'
require 'docker/stats'

describe Docker::Stats do
  it "should have a VERSION constant" do
    expect(subject.const_get('VERSION')).to_not be_empty
  end
end
